# React Example Ingaia

git clone git@github.com:saigowthamr/Reactrouter-tutorials.git
```

### How to Install ?

```
npm install
npm start
```

https://i.stack.imgur.com/AgqTe.png

As caixas azuis são referências a scripts, todos os quais você pode executar diretamente com um npm run <script-name>comando. Mas como você pode ver, na verdade existem apenas 2 fluxos práticos:

npm run start
npm run build
As caixas cinzas são comandos que podem ser executados na linha de comando.

Portanto, por exemplo, se você executar npm start(ou npm run start) que realmente se traduz no npm-run-all -p watch-css start-jscomando, que é executado a partir da linha de comando.

No meu caso, eu tenho esse npm-run-allcomando especial , que é um plugin popular que procura por scripts que começam com "build:" e executa todos esses. Na verdade, não tenho nenhum que corresponda a esse padrão. Mas também possui 2 parâmetros após a -ptroca, que são outros scripts. Então, aqui ele atua como uma abreviação para executar esses 2 scripts. (ie watch-csse start-js)

O watch-cssgarante que os *.scssarquivos sejam traduzidos para *.cssarquivos e procura atualizações futuras.

Os start-jspontos para os react-scripts startquais hospeda o site em um modo de desenvolvimento.

Em conclusão, o npm startcomando é configurável. Se você quiser saber o que faz, verifique o package.jsonarquivo. (e você pode fazer um pequeno diagrama quando as coisas ficarem complicadas).