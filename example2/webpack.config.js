var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require("webpack");
var folder = __dirname;

module.exports = {
    entry: [
      path.resolve('./app/App.jsx')
    ],
    output: {
        path: './public', 
        filename: 'bundle.js'
    },
    devServer: {
        inline: true,
        contentBase: './public',
        port: 3333,
        hot: true
    },
    devtool: 'inline-source-map',
    module: {
        loaders: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
              presets: ['es2015', 'react']
            }
        }]
    },
    resolve: {
      extensions: ['', '.js', '.jsx'],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'public/index.html'
      })
    ],
}