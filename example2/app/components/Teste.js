import React from 'react';

class TesteInput extends React.Component {
    render() {
        return (
          <button className="square">
            {this.props.value}
          </button>
        );
    }   
}

class Teste extends React.Component {
    // render() {
    //   return <h1>Olá, {this.props.name}</h1>;
    // }

    constructor(props) {
        super(props);
        this.valor = 0
        this.state = {date: new Date()};
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    componentDidMount() {
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
    }

    tick() {
        this.setState({
          date: new Date()
        });

        // this.setState((state, props) => ({
        //     counter: state.counter + props.increment
        // }));
    }
    

    testeFunction() {
        alert('Adicionou DOis!');
    }

    render() {
        return (
            <div className="nome-Class" id="teste">
                <h1>Olá, {this.props.name} {this.state.date.toLocaleTimeString()}</h1>
                <p>Teste test 345 5 teste </p>
                <TesteInput value={this.props.name} />
                <button onClick={this.testeFunction}>
                    Clicar aqui
                </button>
            </div>
        );
    }
}


// const Teste = () => <h1>Teste!</h1>

export default Teste;
