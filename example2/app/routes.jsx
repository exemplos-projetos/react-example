import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

//Components
import Teste from './components/Teste'
import Teste2 from './components/Teste2'

const Routes = () => (

  <Router>
    <div>
      <Route exact path="/" component={Teste}/>
      <Route path="/teste" component={Teste2}/>
    </div>
  </Router>

);

export default Routes;
